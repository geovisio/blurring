FROM python:3.9-slim

WORKDIR /opt/blur

# Dependencies
RUN apt update \
	&& apt install -y git gcc ffmpeg libsm6 libxext6 \
 	&& rm -rf /var/lib/apt/lists/*

COPY ./YOLOv6/ ./YOLOv6

COPY ./requirements*.txt ./
RUN pip install -r ./requirements-bin.txt \
	&& pip install -r ./requirements-api.txt


# Source files
COPY ./src ./src
COPY ./tests ./tests
COPY ./docker/docker-entrypoint.sh ./
RUN chmod +x ./docker-entrypoint.sh


# Environment variables
ENV STRATEGY="COMPROMISE"
ENV WEBP_METHOD="6"


# Expose service
EXPOSE 80
ENTRYPOINT ["./docker-entrypoint.sh"]
