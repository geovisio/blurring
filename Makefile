.DEFAULT_GOAL := help

.PHONY: docs

docs:  ## Generates documentation from Typer embedded docs
	cd ./src && python -m typer_cli ./main.py utils docs --name "python src/main.py" --output ../USAGE.md

help: ## Print this help message
	@grep -E '^[a-zA-Z_-]+:.*## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
