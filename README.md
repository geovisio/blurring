# ![Panoramax](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/40px-Panoramax.svg.png) Panoramax

__Panoramax__ is a digital resource for sharing and exploiting 📍📷 field photos. Anyone can take photographs of places visible from the public streets and contribute them to the Panoramax database. This data is then freely accessible and reusable by all. More information available at [gitlab.com/panoramax](https://gitlab.com/panoramax) and [panoramax.fr](https://panoramax.fr/).


# 🪄🫥 GeoVisio Blurring API

This repository only contains __the blurring algorithms and its API__.

__IMPORTANT NOTE__ : this repository hosts the first, older, __unmaintained__ version of Panoramax' blurring API. It is not used anymore, and all running instances are using [SGBlur](https://gitlab.com/panoramax/server/sgblur) instead. You may check out SGBlur instead for a better performance and maintained code.

## Install

### System dependencies

Some algorithms (compromise and qualitative) will need system dependencies :

- ffmpeg
- libsm6
- libxext6

You can install them through your package manager, for example in Ubuntu:

```bash
sudo apt install ffmpeg libsm6 libxext6
```

### Retrieve code

You can download code from this repository with git clone:

```bash
git clone https://gitlab.com/panoramax/server/blurring.git
cd blurring/
```

### Other dependencies

We use [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to manage some of our dependencies. Run the following command to get these dependencies:

```bash
git submodule update --init
```

We also use Pip to handle Python dependencies. You can create a virtual environment first:

```bash
python -m venv env
source ./env/bin/activate
```

And depending on if you want to use API, command-line scripts or both, run these commands:

```bash
pip install -r requirements-bin.txt  # For CLI
pip install -r requirements-api.txt  # For API
```

If at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/panoramax/server/blurring/-/issues) or by [email](mailto:panieravide@riseup.net).


## Usage

### Command-line interface

All details of available commands are listed in [USAGE.md](./USAGE.md) documentation, or by calling this command:

```bash
python src/main.py --help
```

A single picture can be blurred using the following command:

```bash
python src/main.py <path the the picture> <path to the output picture>
```

You can also launch the CLI through Docker:

```bash
docker run \
	geovisio/blurring \
	cli
```

### Web API

The Web API can be launched with the following command:

```bash
uvicorn src.api:app --reload
```

It is then accessible on [localhost:8000](http://127.0.0.1:8000).

You can also launch the API through Docker:

```bash
docker run \
	-p 8000:80 \
	--name geovisio_blurring \
	geovisio/blurring \
	api
```

API documentation is available under `/docs` route, so [localhost:8000/docs](http://127.0.0.1:8000/docs) if you use local instance.

A single picture can be blurred using the following HTTP call (here made using _curl_):

```bash
# Considering your picture is called my_picture.jpg
curl -X 'POST' \
  'http://127.0.0.1:8000/blur/' \
  -H 'accept: image/webp' \
  -H 'Content-Type: multipart/form-data' \
  -F 'picture=@my_picture.jpg;type=image/jpeg' \
  --output blurred.webp
```

Note that various settings can be changed to control API behaviour. You can edit them [using one of the described method in FastAPI documentation](https://fastapi.tiangolo.com/advanced/settings/). Available settings are:

- `STRATEGY`: blur algorithm to use (FAST, LEGACY, COMPROMISE, QUALITATIVE)
- `WEBP_METHOD`: quality/speed trade-off for WebP encoding of pictures derivates (0=fast, 6=slower-better, 6 by default)


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

You might want to read more [about available blur algorithms](./ALGORITHMS.md).

### Testing

Tests are handled with Pytest. You can run them using:

```bash
pip install -r requirements-dev.txt
pytest
```

### Documentation

High-level documentation for command-line script is handled by [Typer](https://typer.tiangolo.com/). You can update the generated `USAGE.md` file using this command:

```bash
make docs
```


## ⚖️ License

Copyright (c) GeoVisio team 2022-2023, [released under MIT license](https://gitlab.com/panoramax/server/blurring/-/blob/develop/LICENSE).
