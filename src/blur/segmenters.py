import os
import numpy as np
from PIL import Image
import tarfile

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # disable non-error logs
import tensorflow.compat.v1 as tf

from . import precess


class Segmenter(object):
    """An image sementic segmentation model"""

    def computeBlurMask(self, picture):
        raise Exception("Unimplemented")

    def getPreferedInputSize(self):
        raise Exception("Unimplemented")


    @staticmethod
    def makeSegmentationColormap(labelNames, detectedClasses):
        """Returns a colormap usable by segmentation models

        Parameters
        ----------
        labelNames : [str]
            the list of labels known to the model, in order
        detectedClasses : [str]
            the list of classes (labels) that will be kept

        Returns
        -------
        np.array
            a colormap
        """
        colormap = np.zeros((len(labelNames), 4), dtype=int)
        for clazz in detectedClasses:
            index = labelNames.index(clazz)
            colormap[index] = (255,255,255,255)
        return colormap


class FastSegmenter(Segmenter):
    """Fast segmenter, uses tensorflow lite and a pretrained model.

    Instances of this class __cannot__ be shared between threads.

    Used model: https://tfhub.dev/sayakpaul/lite-model/mobilenetv2-dm05-coco/dr/1
    """

    MODEL_NAME = 'lite-model_mobilenetv2-dm05-coco_dr_1.tflite'
    DOWNLOAD_URL = 'https://tfhub.dev/sayakpaul/lite-model/mobilenetv2-dm05-coco/dr/1?lite-format=tflite'
    LABEL_NAMES = [ 'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tv' ]
    BLURRED_CLASSES = [ 'bus', 'car', 'person', 'motorbike' ]
    BATCH_SIZE = 2

    def __init__(self):
        interpreter = tf.lite.Interpreter(model_path=precess.getModelFile(self.MODEL_NAME))
        inputDetails = interpreter.get_input_details()
        tensorIndex = inputDetails[0]['index']
        tensorShape = inputDetails[0]['shape']
        interpreter.allocate_tensors()

        self.inputSize = tensorShape[2], tensorShape[1]
        self.interpreter = interpreter
        self.inputTensorName = interpreter.get_input_details()[0]['index']
        self.outputTensorName = interpreter.get_output_details()[0]['index']
        self.blurColormap = self.makeSegmentationColormap(self.LABEL_NAMES, self.BLURRED_CLASSES)


    def runSegmentation(self, image):
        if image.size != self.inputSize:
            raise RuntimeError("Invalid input: got size %s but expected %s" % (str(image.size), str(self.inputSize)))

        inputImage = np.asarray(image).astype(np.float32)
        inputImage = np.expand_dims(inputImage, 0)
        inputImage = inputImage / 127.5 - 1

        self.interpreter.set_tensor(self.inputTensorName, inputImage)
        self.interpreter.invoke()
        rawPrediction = self.interpreter.get_tensor(self.outputTensorName)[0]
        segMap = tf.argmax(rawPrediction, axis=2)
        segMap = tf.squeeze(segMap).numpy().astype(np.int8)
        segMap = Image.fromarray(np.uint8(self.blurColormap[segMap]))

        return segMap

    def getPreferedInputSize(self):
        return self.inputSize


class PreciseSegmenter(Segmenter):
    """Precise segmenter, uses tensorflow and a pretrained model.

    A single instance of this class can be shared between threads.

    By default the model will try to use all available CPUs, this may
    cause severe performance issues and even crashes on lower-end
    computers.

    Model and code sample found at: https://averdones.github.io/real-time-semantic-image-segmentation-with-deeplab-in-tensorflow/
    """

    MODEL_NAME = 'deeplabv3_pascal_trainval_2018_01_04.tar.gz'
    DOWNLOAD_URL = 'http://download.tensorflow.org/models/deeplabv3_pascal_trainval_2018_01_04.tar.gz'
    LABEL_NAMES = [ 'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tv' ]
    BLURRED_CLASSES = [ 'bus', 'car', 'motorbike', 'person' ]

    FROZEN_GRAPH_NAME = 'frozen_inference_graph'
    INPUT_TENSOR_NAME = 'ImageTensor:0'
    OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
    INPUT_SIZE = 513

    def __init__(self):
        self.graph = tf.Graph()

        # Extract frozen graph from tar archive.
        graphDef = None
        tarFile = tarfile.open(precess.getModelFile(self.MODEL_NAME))
        for tarInfo in tarFile.getmembers():
            if self.FROZEN_GRAPH_NAME in os.path.basename(tarInfo.name):
                fileHandle = tarFile.extractfile(tarInfo)
                graphDef = tf.GraphDef.FromString(fileHandle.read())
                break
        tarFile.close()
        if graphDef is None:
            raise RuntimeError('Cannot find inference graph in tar archive.')

        with self.graph.as_default():
            tf.import_graph_def(graphDef, name='')

        self.sess = tf.Session(graph=self.graph)
        self.blurColormap = self.makeSegmentationColormap(self.LABEL_NAMES, self.BLURRED_CLASSES)


    def runSegmentation(self, image):
        if image.size != self.getPreferedInputSize():
            raise RuntimeError("Invalid input: got size %s but expected %s" % (str(image.size), str(self.getPreferedInputSize())))

        feedDict = { self.INPUT_TENSOR_NAME: [ np.asarray(image) ] }
        batchSegmentationMap = self.sess.run(self.OUTPUT_TENSOR_NAME, feedDict)
        segmentationMap = batchSegmentationMap[0]
        return Image.fromarray(np.uint8(self.blurColormap[segmentationMap]))

    def getPreferedInputSize(self):
        return (self.INPUT_SIZE, self.INPUT_SIZE)
