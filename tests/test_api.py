from fastapi.testclient import TestClient
from PIL import Image, ImageChops, ImageStat
from io import BytesIO
from src.api import app
from . import conftest

client = TestClient(app)


def test_get_root():
	response = client.get("/")
	assert response.status_code == 200
	assert response.json() == {"message": "GeoVisio Blurring API"}


@conftest.IMG1
@conftest.MASK1
def test_post_mask_valid(datafiles):
	with open(datafiles + "/1.jpg", "rb") as f:
		response = client.post("/mask", files={"picture": f})

		assert response.status_code == 200
		assert response.headers.get("content-type") == "image/png"

		mask = Image.open(BytesIO(response.content))

		with open(datafiles + "/1_mask.png", "rb") as expectedMaskFile:
			expectedMask = Image.open(expectedMaskFile)
			diff = ImageChops.difference(mask, expectedMask)
			stat = ImageStat.Stat(diff)
			diff_ratio = sum(stat.mean) / (len(stat.mean) * 255) * 100
			assert diff_ratio <= 5 # Less than 5% difference


@conftest.IMG1
@conftest.BLUR1
def test_post_blur_valid(datafiles):
	with open(datafiles + "/1.jpg", "rb") as f:
		response = client.post("/blur", files={"picture": f})

		assert response.status_code == 200
		assert response.headers.get("content-type") == "image/webp"

		blur = Image.open(BytesIO(response.content))

		with open(datafiles + "/1_blurred.webp", "rb") as expectedBlurFile:
			expectedBlur = Image.open(expectedBlurFile)
			diff = ImageChops.difference(blur, expectedBlur)
			stat = ImageStat.Stat(diff)
			diff_ratio = sum(stat.mean) / (len(stat.mean) * 255) * 100
			assert diff_ratio <= 5 # Less than 5% difference
